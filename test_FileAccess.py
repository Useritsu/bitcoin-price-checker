import unittest
import main

class CalcTest(unittest.TestCase):
    def test_error_on_empty_file(self):
        results = main.calc([])
        self.assertEqual(results, None)

    def test_percentage_increase(self):
        results = main.calc([('2017-20-20', 4), ('2017-10-10', 100)])
        self.assertEqual(results[1][0],True)
        self.assertEqual(results[2][0],False)
    
    def test_percentage_decrease(self):    
        results = main.calc([('2017-20-20', 4), ('2017-10-10', 6)])
        self.assertEqual(results[2][1],0)


if __name__ == "__main__":
    unittest.main()
