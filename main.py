from __future__ import print_function
import sys

def loadCSV(filename):
    data = []

    try:
        file = open(filename, "r")
    except:
        print("Failed to open "+ filename)
        sys.exit(-1)

    for line in file:
        x = line.split(',')
        day = x[0][:10]
        price = float(x[1].strip())
        data.append((day, price))

    return data

def calc(array):
    if len(array) == 0:
        return None

    previousPrice = -1
    highestDay = ""
    highestPrice = 0
    highestPercIncrease = 0
    highestPercIncreaseDay = "" 
    highestPercDecrease = 0
    highestPercDecreaseDay = ""
    hadDecrease = False
    hadIncrease = False
    
    for i in range(len(array)):
        x = array[i]
        day = x[0]
        price = x[1]

        if previousPrice != -1:
            delta = 100 * (price - previousPrice) / previousPrice

            if delta < 0:
                hadDecrease = True
            if delta > 0:
                hadIncrease = True

            if delta > highestPercIncrease:
                highestPercIncrease = delta
                highestPercIncreaseDay = day
            if delta < highestPercDecrease:
                highestPercDecrease = delta
                highestPercDecreaseDay = day
         
            print("{:.10}: {:4.0f} USD  {:+3.1f}%".format(day, price, delta))

        if price > highestPrice:
            highestPrice = price
            highestDay = day

        previousPrice = price

    return [(highestPrice, highestDay),
            (hadIncrease, highestPercIncrease, highestPercIncreaseDay),
            (hadDecrease, highestPercDecrease, highestPercDecreaseDay)]

        
def main():
    if len(sys.argv)!=2:
        print("Usage: main.py FILE")
        sys.exit(0)  

    filename = sys.argv[1]
    results = calc(loadCSV(filename))
    if results == None:
        print("No data in file")
        sys.exit(-1)
        
    print('highest price was {} on {}'.format(results[0][0], results[0][1]))
    if results[1][0]:
        print('highest % increase was {:+3.1f}% on {}'.format(results[1][1], results[1][2]))
    else:
        print("There were no increases in the data")
    if hadDecrease:
        print('highest % decrease was {:+3.1f}% on {}'.format(results[2][1],results[2][2]))
    else:
        print("There were no decreases in the data")


if __name__ == '__main__':
    main()
