# My project's README


The code does basic calculations on CSV files and gives you relevant data about it.
It works with both python 2 and python 3.

How to use:

In the terminal, you initialize the program by writing:

python3 main.py -pass in argument- as the csv file.


Example python3 main.py marketprice.csv

Currently, it's set to work only with CSV's from blockchain.info.